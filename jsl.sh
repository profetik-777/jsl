#In the event package name changes use the following
#to search  type in terminal: apt-cache search xyz_software_name
#to run script - sudo sh ./jsh.sh


#############     SYSTEM TOOLS and TWEAKS    ###############

#PERSONAL SYSTEM TWEAKS

#Tailscale VPN, but first install curl just in case it isn't already installed.
sudo apt install curl -y 

#Instal tailscale 
curl -fsSL https://tailscale.com/install.sh | sh

#ssh 
sudo apt install openssh-server -y
systemctl enable ssh.service
systemctl start ssh.service

#xkill
sudo apt install xkill -y

#network monitoring tools
sudo apt install net-tools -y

#hardware info app
sudo apt install inxi -y 

#displaylink installer for wavlink hub
#add to repo
sudo apt install ./Downloads/synaptics-repository-keyring.deb -y
sudo apt install displaylink-driver -y

############      #APPS        #################


##### Regular Apt Installs

sudo apt install tilix -y 
sudo apt install audacity -y
sudo apt install neofetch -y
sudo apt install pavucontrol -y
sudo apt install vlc -y
sudo apt install synaptic -y
sudo apt install zim -y
sudo apt install cheese -y
#sudo apt install kazam -y
#sudo apt install atom -y
#sudo apt install chromium -y
#sudo apt install hexchat -y
#sudo apt install polari -y
#sudo apt install brasero -y
#sudo apt install handbrake -y
#sudo apt install transmission -y
sudo apt install mumble -y
sudo apt install torbrowser-launcher -y
#sudo apt install libreoffice -y
sudo apt install focuswriter -y
sudo apt install simple-scan -y
sudo apt install gscan2pdf -y
sudo apt install xpaint -y
sudo apt install discord -y
sudo apt install gdebi -y
sudo apt install x11utils -y
sudo apt install pdfsam -y 
sudo apt install kdenlive -y
sudo apt install openshot -y
sudo apt install disks -y
sudo apt install xournal -y
#sudo apt install x2goclient - y
sudo apt install x11vnc -y
sudo apt install virtualbox -y
sudo apt install build-essential dkms linux-headers-$(uname -r)

###### FLATPAKS ########

#enable flathub
flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

#for gnome extension management
flatpak install flathub com.mattjakeman.ExtensionManager -y

#Flatpak Apps

#flatpak install flathub io.wavebox.Wavebox -y
#flatpak install flathub com.google.AndroidStudio -y
flatpak install flathub com.obsproject.Studio -y
flatpak install telegram org.telegram.desktop -y

###### WGET downloads Section
#chrome
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb



#Install standardnotes_appimage with right configurations for easy access. 
#Not really using this anymore - since can't get current download due to 
#version numbering. Figure out if they have latest version by basic naming for download 
#eg standardnotes_current like other projects do
#wget https://standardnotes.org/extensions?downloaded=linux
#move it to a more perm directory
#mv ~/Downloads/Standard-Notes-3.4.1.AppImage ~/Standard-Notes-3.4.1.AppImage
#cd ~
#give it the right permissions
#chmod a+x Standard-Notes-3.4.1.AppImage

#wavebox (for better performance, best to use this than snap version)
#sudo wget -qO - https://wavebox.io/dl/client/repo/archive.key | sudo apt-key add -
#echo "deb https://wavebox.io/dl/client/repo/ x86_64/" | sudo tee --append /etc/apt/sources.list.d/wavebox.list
#sudo apt update
#sudo apt install wavebox -y

#anydesk
#wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | apt-key add -
#echo "deb http://deb.anydesk.com/ all main" > /etc/apt/sources.list.d/anydesk-stable.list
#sudo apt update
#apt install anydesk -y

#brave-nightly
#sudo apt install apt-transport-https curl
#curl -s https://brave-browser-apt-nightly.s3.brave.com/brave-core-nightly.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-prerelease.gpg add -
#echo "deb [arch=amd64] https://brave-browser-apt-nightly.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-nightly.list
#sudo apt update
#sudo apt install brave-browser-nightly -y

brave-stable
sudo apt install apt-transport-https curl
curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -
echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update
sudo apt install brave-browser -y

#zoom
wget https://zoom.us/client/latest/zoom_amd64.deb
sudo dpkg -i zoom_amd64.deb 

######### SNAPS (no longer used but in case I give it another chance#############
#wavebox and mailspring. 
#sudo snap install obs-studio 
#sudo snap install telegram-desktop
#sudo snap install thunderbird --beta
#sudo snap install mailspring
#sudo snap install wavebox
#CoolRetroTerminal
#sudo snap install cool-retro-term -y

#insync 
#https://www.insynchq.com/downloads?start=true 



#Archived - Needed for old job

#VPN ANYCONNECT by CISCO backend
#sudo apt install openconnect -y

#to have it show in menu to add
#sudo apt install network-manager-openconnect -y

#This enables you to configure your openconnect vpn, not just show it as an option
#sudo apt install network-manager-openconnect-gnome -y

#This enables you to use ScreenConnect/Connect Wise which leverages java web start technology 
#sudo apt install icedtea-netx -y